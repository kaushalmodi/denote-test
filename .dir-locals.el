((nil . ((indent-tabs-mode . nil)
         (fill-column . 70)
         (sentence-end-double-space . t)
         ;; (eval . (setq-local denote-directory (file-name-directory (buffer-file-name))))
         (denote-front-matter-date-format . org-timestamp)))
 (org-mode . ((eval . (auto-fill-mode))
              (denote-org-front-matter . ":PROPERTIES:
:ID: %4$s
:END:
#+title:    %1$s
#+date:     %2$s
#+filetags: %3$s
\n"))))
